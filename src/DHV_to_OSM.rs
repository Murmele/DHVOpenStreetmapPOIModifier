use xmltree::{Element, XMLNode};
// https://wiki.openstreetmap.org/wiki/OsmChange
// https://donatstudios.com/CsvToMarkdownTable
// | Desc                                      | OSM                                                     | DHV                                                 | 
// |-------------------------------------------|---------------------------------------------------------|-----------------------------------------------------| 
// | Altitude                                  | ele                                                     | Altitude                                            | 
// | Hanggliding possibility                   | free_flying:hanggliding=yes/no                          | Hanggliding                                         | 
// | Paraglide possibility                     | free_flying:paragliding=yes/no                          | Paragliding                                         | 
// | DHV Reference page                        | free_flying:dhv:referencepage                           | SiteUrl                                             | 
// | Annotations for hole DHV area of one spot | free_flying:annotations:de                              | SiteRemarks                                         | 
// | Name                                      | name                                                    | SiteName                                            | 
// | Website                                   | website                                                 |                                                     | 
// | Requirements                              | free_flying:requirements:de                             | Requirements                                        | 
// | Remarks special for an exact location     | free_flying:annotations:de (appended?)                  | LocationRemarks                                     | 
// | Orientation (convert letters to english)  | free_flying:site_orientation                            | DirectionsText                                      | 
// | Starting/Landing/…                        | free_flying:site (takeoff, landing, towing, toplanding) | LocationType, 1: Start, 2: Landing, 3: Schleppwinde | 


// Creating a new modify changeset. The node is than stored internally in the object
pub fn createModifyChangeSet(dhv_site_element: &Element, dhv_location_element: &Element, osm_element: &Element) -> Element {
    let mut res = Element::new("node");

    res.attributes.insert("lat".to_string(), osm_element.attributes.get("lat").unwrap().to_string());
    res.attributes.insert("lon".to_string(), osm_element.attributes.get("lon").unwrap().to_string());
    res.attributes.insert("id".to_string(), osm_element.attributes.get("id").unwrap().to_string());

    addTagIfNotExistString(& mut res, dhv_location_element, "Altitude", osm_element, "ele");
    addTagIfNotExistString(& mut res, dhv_location_element, "Hanggliding", osm_element, "free_flying:hanggliding");
    addTagIfNotExistString(& mut res, dhv_location_element, "Paragliding", osm_element, "free_flying:paragliding");
    
    if let Some(url) = dhv_value(dhv_site_element, "SiteUrl") {
        addTagIfNotExist(& mut res, &fixUrl(&url), osm_element, "free_flying:dhv:referencepage");
    }

    if let Some(req) = dhv_value(dhv_site_element, "SiteRemarks") {
        addTagIfNotExist(& mut res, &cleanHTMLText(&req), osm_element, "free_flying:annotations:de");
    }
    
    addTagIfNotExistString(& mut res, dhv_site_element, "SiteName", osm_element, "name");
    // addTagIfNotExistString(& mut res, dhv_location_element, "Altitude", osm_element, "website");
    
    if let Some(req) = dhv_value(dhv_site_element, "Requirements") {
        addTagIfNotExist(& mut res, &cleanHTMLText(&req), osm_element, "free_flying:requirements:de");
    }
    
    // addTagIfNotExistString(& mut res, dhv_location_element, "LocationRemarks", osm_element, "free_flying:annotations:de (appended?)");

    if let Some(directions) = dhv_value(dhv_location_element, "DirectionsText") {
            addTagIfNotExist(& mut res, &dhvDirectionsTextToOsmOrientation(&directions), osm_element, "free_flying:site_orientation");
    }

    if let Some(site) = dhv_value(dhv_location_element, "LocationType") {
        if let Some(site) = dhvLocationTypeToOsmSite(site.parse::<u32>().unwrap()) {
            addTagIfNotExist(& mut res, &site, osm_element, "free_flying:site");
        }
    }

    return res;
}

// Creating a new add changeset. The node is than stored internally in the object
// Use this if you would like to create a new node
pub fn createAddChangeSet(dhv_site_element: &Element, dhv_location_element: &Element) -> Element {
    let mut res = Element::new("node");

    let text = dhv_location_element.get_child("Coordinates").unwrap().get_text().unwrap();
    let coords = text.split(",");
    let coords: Vec<&str> = coords.collect();
    let lon = coords[0];
    let lat = coords[1];
    res.attributes.insert("lat".to_string(), lat.to_string());
    res.attributes.insert("lon".to_string(), lon.to_string());
    res.attributes.insert("version".to_string(), "1".to_string());

    res.attributes.insert("ele".to_string(), dhv_location_element.get_child("Altitude").unwrap().get_text().unwrap().to_string());
    res.attributes.insert("free_flying:hanggliding".to_string(), dhv_location_element.get_child("Hanggliding").unwrap().get_text().unwrap().to_string());
    res.attributes.insert("free_flying:paragliding".to_string(), dhv_location_element.get_child("Paragliding").unwrap().get_text().unwrap().to_string());
    
    if let Some(url) = dhv_value(dhv_site_element, "SiteUrl") {
        res.attributes.insert("free_flying:dhv:referencepage".to_string(), fixUrl(&url));
    }

    if let Some(req) = dhv_value(dhv_site_element, "SiteRemarks") {
        res.attributes.insert("free_flying:annotations:de".to_string(), cleanHTMLText(&req));
    }
    
    res.attributes.insert("name".to_string(), dhv_site_element.get_child("SiteName").unwrap().get_text().unwrap().to_string());
    // addTagIfNotExistString(& mut res, dhv_location_element, "Altitude", osm_element, "website");
    
    if let Some(req) = dhv_value(dhv_site_element, "Requirements") {
        res.attributes.insert("free_flying:requirements:de".to_string(), cleanHTMLText(&req));
    }
    
    // addTagIfNotExistString(& mut res, dhv_location_element, "LocationRemarks", osm_element, "free_flying:annotations:de (appended?)");

    if let Some(directions) = dhv_value(dhv_location_element, "DirectionsText") {
            res.attributes.insert("free_flying:site_orientation".to_string(), dhvDirectionsTextToOsmOrientation(&directions));
    }

    if let Some(site) = dhv_value(dhv_location_element, "LocationType") {
        if let Some(site) = dhvLocationTypeToOsmSite(site.parse::<u32>().unwrap()) {
            res.attributes.insert("free_flying:site".to_string(), site);
        }
    }

    return res;
}

fn fixUrl(url: &str) -> String {
    url.to_string()
}

fn cleanHTMLText(text: &str) -> String {
    text.to_string()
}

fn dhvDirectionsTextToOsmOrientation(directions: &str) ->String {
    directions.to_string().replace("O", "E")
}

fn dhv_value(dhv_element: &Element, dhv_tag_name: &str) -> Option<String> {
    if let Some(v) = dhv_element.get_child(dhv_tag_name) {
        if let Some(text) = v.get_text() {
            return Some(text.to_string());
        }
    } else {
        //println!("DHV Element {dhv_location_id} does not have a tag named: {tag}", dhv_location_id=)
        match dhv_element.get_child("SiteID") {
            Some(v) => println!("DHV Element with LocationID {} does not have a tag named {}", v.get_text().unwrap(), dhv_tag_name),
            None => println!("DHV Element with LocationID {} does not have a tag named {}", dhv_element.get_child("LocationID").unwrap().get_text().unwrap(), dhv_tag_name) 
        }
    }    
    None
}

fn addTagIfNotExistString(node: &mut Element, dhv_element: &Element, dhv_tag_name: &str, osm_element: &Element, osm_tag_name: &str) {
    if let Some(v) =dhv_value(dhv_element, dhv_tag_name) {
        addTagIfNotExist(node, &v, osm_element, osm_tag_name);
    }
}

fn addTagIfNotExist(node: &mut Element, dhv_value: &str, osm_element: &Element, osm_tag_name: &str) {
    //node.children.push();

    if let None = osm_element.get_child(osm_tag_name) {
        // Child does not exist yet, so add the data from the dhv element

        let mut tag = Element::new("tag");
        tag.attributes.insert(osm_tag_name.to_string(), dhv_value.to_string());
        node.children.push(XMLNode::Element(tag));
    } else {
        println!("OSM Element {osm_id}: Tag {tag} already exists.", osm_id=osm_element.attributes.get("id").unwrap(), tag=osm_tag_name);
    }
}

fn dhvLocationTypeToOsmSite(location_type: u32) -> Option<String> {
    match location_type {
        1 => Some("Start".to_string()),
        2 => Some("Landing".to_string()),
        3 => Some("Towing".to_string()),
        _ => None
    }
}