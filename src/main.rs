use xmltree::{Element};
use std::fs::{File, read_to_string};
use std::io::{Write};
use std::f64::consts::PI;
//use failure;

use osm_api::{osmapi, overpass};
mod DHV_to_OSM;

const free_flying_cache_file: &str = "free_flying_cache_file.xml";
const dhvgelaende_dhvxml_alle_cached: &str = "dhvgelaende_dhvxml_alle.xml";

fn main() {

    let dev = true;
    let mut account = osmapi::OsmAccount::new(String::from(""), String::from(""), dev);

    account.createChangeSet("Test creating a changeset");

    // Read OSM data
    let osm_element = Element::parse(fetch_free_flying_osm_data().unwrap().as_bytes()).unwrap();

    // Read DHV file
    let dhv_element = Element::parse(fetch_dhv_database().unwrap().as_bytes()).unwrap();
    let dhv_flying_sites = dhv_element.get_child("FlyingSites").unwrap();

    

    let mut count = 0;
    let mut new_id: i32 = -1;

    for dhv_site in &dhv_flying_sites.children { // Iterate over all OSM elements
        if count > 20 {
            break;
        }
        let dhv_site_element = dhv_site.as_element().unwrap();
        for child in &dhv_site_element.children {
            let dhv_location_element = child.as_element().unwrap();
            if !dhv_location_element.name.eq("Location") {
                continue;
            }

            // every location has at least one Coordinates child
            let text = dhv_location_element.get_child("Coordinates").unwrap().get_text().unwrap();
            let coords = text.split(",");
            let coords: Vec<&str> = coords.collect();
            let lon = coords[0].parse::<f64>().unwrap();
            let lat = coords[1].parse::<f64>().unwrap();

            // Now lets find the corresponding node in the osm file
            let mut matched = false;
            for n in &osm_element.children { // Iterate over all osm elements
                let osm_node = n.as_element().unwrap();

                if !osm_node.name.eq("node") {
                    continue;
                }
                let lon_osm = osm_node.attributes.get("lon").unwrap().parse::<f64>().unwrap();
                let lat_osm = osm_node.attributes.get("lat").unwrap().parse::<f64>().unwrap();

                if distance(lat, lon, lat_osm, lon_osm) < 30_f64 {
                    println!("Match");
                    // TODO: change version
                    let version = "1";
                    account.add_modify_node_changeset(DHV_to_OSM::createModifyChangeSet(dhv_site_element, dhv_location_element, osm_node), version);
                    matched = true;
                    count += 1;
                    break;
                }
            }
            if !matched {
                let changeset_id = account.changeset_id().unwrap().clone();
                account.add_create_node_changeset(DHV_to_OSM::createAddChangeSet(dhv_site_element, dhv_location_element));
                new_id -= 1;
                count += 1;
            }
        }
    }
    account.write_changeset_to_file("changeset.xml").unwrap();
    //account.put(sub_url: &str)
}

fn distance(lat1: f64, lon1: f64, lat2: f64, lon2: f64) -> f64 {
    let earth_radius = 6378.137; // [km]
    let dLat = (lat2 - lat1) * PI/180.0;
    let dLon = (lon2 - lon1) * PI/180.0;

    let a = (dLat/2_f64).sin() * (dLat/2_f64).sin() + (lat1*PI/180_f64).cos() * (lat2*PI/180_f64).cos() * (dLon/2_f64).sin() * (dLon/2_f64).sin();
    let c = 2.0 * a.sqrt().atan2((1_f64-a).sqrt());
    let d = earth_radius * c;
    return d * 1000_f64 // meters
    // function measure(lat1, lon1, lat2, lon2){  // generally used geo measurement function
    //     var dLat = lat2 * Math.PI / 180 - lat1 * Math.PI / 180;
    //     var dLon = lon2 * Math.PI / 180 - lon1 * Math.PI / 180;
    //     var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
    //     Math.cos(lat1 * Math.PI / 180) * Math.cos(lat2 * Math.PI / 180) *
    //     Math.sin(dLon/2) * Math.sin(dLon/2);
    //     var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
    //     var d = R * c;
    //     return d * 1000; // meters
    // }
}

// #[derive(failure::Fail, Debug)]
// #[fail(display = "There is an error: {}.", _0)]
// struct DHVDatabaseFetchError(String);

fn fetch_dhv_database() -> Result<String, Box<dyn std::error::Error>> {
    if std::path::Path::new(dhvgelaende_dhvxml_alle_cached).exists() {
        return Ok(read_to_string(dhvgelaende_dhvxml_alle_cached)?);
    }
    
    //Err(Box::new(DHVDatabaseFetchError("Fetching database from the DHV website is not yet supported.".to_string())))
    Ok("".to_string())
}

fn fetch_free_flying_osm_data() -> Result<String, Box<dyn std::error::Error>> {
    if std::path::Path::new(free_flying_cache_file).exists() {
        return Ok(read_to_string(free_flying_cache_file)?);
    }

    let query = "node[\"sport\"=\"free_flying\"];";
    let api = overpass::API::new("https://lz4.overpass-api.de/api/interpreter", 200);
    let result = api.get(query, overpass::Responseformat::XML,  "body", true);
    match result {
        Err(e) => return Err(e),
        Ok(t) => {
            export_free_flying_to_file(&t, free_flying_cache_file);
            return Ok(t.to_string())
        }
    }
}

fn export_free_flying_to_file(text: &str, file: &str) {
    // Write result to file so that it is cached.
    let output = File::create(file);
    match output {
        Err(e) => println!("Failed to open file: {}", e),
        Ok(mut o) => {
            match write!(o, "{}", text) {
                Err(e) => println!("Failed to write to file: {}", e),
                Ok(_) => println!("Successfully written file.")

            }
        }
    }
}
